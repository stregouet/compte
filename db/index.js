import client from './client'
import server from './server'

export default async function db(verb, path, ctx = {}) {
  let module
  const key = `${verb.toLowerCase()}://${path.split('?')[0]}`
  if (process.client) {
    module = client
  } else {
    module = server
  }
  if (module.has(key)) {
    const handler = module.get(key)
    return handler(ctx)
  }
  throw new Error(`unknown route: "${key}", ${Array.from(module.keys())}`)
}
