import * as pg from 'pg'
import format from 'date-fns/format'

const pool = new pg.Pool()

function formatDate(d) {
  return format(d, 'YYYY-MM-DD')
}

async function get(ctx) {
  const { rows } = await pool.query('select * from operations')
  return rows.map(item => ({ ...item, date: formatDate(item.date) }))
}

async function search(ctx) {
  const where = [],
    params = []
  ctx.query = ctx.query || {}
  const { detail, category } = ctx.query
  let q = 'select op.* from operations op'
  if (category) {
    params.push(category)
    q += ', unnest(op.categories) cat'
    where.push(`cat ilike '%' || $${params.length} || '%'`)
  }
  if (detail) {
    params.push(detail)
    where.push(`detail ilike '%' || $${params.length} || '%'`)
  }
  q = `${q} ${where.length ? 'where' : ''} ${where.join(
    ' AND '
  )} order by op.date desc`
  console.log('q', q, params)
  const { rows } = await pool.query(q, params)
  return rows.map(item => ({ ...item, date: formatDate(item.date) }))
}

async function updateCategory(ctx) {
  const { ids, newCategories } = ctx.body
  const { rows } = await pool.query(
    `update operations set categories = '{${newCategories.join(',')}}'` +
      `where id in (${ids.join(',')}) returning *`
  )
  return rows.map(item => ({ ...item, date: formatDate(item.date) }))
}

async function updateOperation(ctx) {
  const { description, id } = ctx.body
  const { rows } = await pool.query(
    'update operations set description = $1 where id = $2 returning * ',
    [description, id]
  )
  return rows[0]
}

const routes = new Map()

routes.set(`get:///operations`, search)
routes.set(`get:///operations/search`, search)
routes.set(`post:///operations/categories`, updateCategory)
routes.set(`post:///operation`, updateOperation)

export default routes
