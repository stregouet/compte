async function get(ctx) {
  let q = ''
  const parts = []
  if (ctx.query) {
    for (let key in ctx.query) {
      const value = ctx.query[key]
      if (!value) {
        continue
      }
      parts.push([key, value])
      q += `${key}=${ctx.query[key]}`
    }
    if (parts.length) {
      q = '?' + parts.map(([k, v]) => `${k}=${v}`).join('&')
    }
  }
  return fetch(`/api/operations${q}`, { credential: 'same-origin' }).then(r =>
    r.json()
  )
}

async function updateCategories({ ids, newCategories }) {
  return fetch('/api/operations/categories', {
    method: 'post',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify({ ids, newCategories }),
    credential: 'same-origin'
  }).then(r => r.json())
}

async function updateOperation({ id, description }) {
  return fetch('/api/operation', {
    method: 'post',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify({ id, description }),
    credential: 'same-origin'
  }).then(r => r.json())
}

const routes = new Map()
routes.set('get:///operations', get)
routes.set('post:///operations/categories', updateCategories)
routes.set('post:///operation', updateOperation)

export default routes
