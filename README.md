# Compte

Application to manage your bank transaction records. 

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on Nuxt.js, checkout its [docs](https://nuxtjs.org).
