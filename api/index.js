import db from '../db'

export default function(req, res, next) {
  console.log('req.path', req.path)
  db(req.method, req.path, req)
    .then(rows => res.send(rows))
    .catch(next)
}
