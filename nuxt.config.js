const path = require('path')
const pkg = require('./package')
const webpack = require('webpack')

require('dotenv').config()

module.exports = {
  server: {
    port: 8000,
    host: '0.0.0.0'
  },
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/material-icon-roboto.css' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['~/assets/style/app.styl'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ['@/plugins/vuetify'],

  serverMiddleware: [{ path: '/api', handler: '~/api/index.js' }],

  /*
  ** Nuxt.js modules
  */
  modules: ['@nuxtjs/dotenv'],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (ctx.isClient) {
        config.module.rules.push({
          test: path.resolve(__dirname, 'db/server.js'),
          use: 'null-loader'
        })
      }
    }
  }
}
